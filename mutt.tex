\documentclass{lug}

\usepackage{fontawesome}
\usepackage{etoolbox}
\usepackage{textcomp}
\usepackage[nodisplayskipstretch]{setspace}
\usepackage{xspace}
\usepackage{verbatim}
\usepackage{multicol}
\usepackage{soul}

\usepackage{amsmath,amssymb,amsthm}

\usepackage[linesnumbered,commentsnumbered,ruled,vlined]{algorithm2e}
\newcommand\mycommfont[1]{\footnotesize\ttfamily\textcolor{blue}{#1}}
\SetCommentSty{mycommfont}
\SetKwComment{tcc}{ \# }{}
\SetKwComment{tcp}{ \# }{}

\usepackage{siunitx}

\usepackage{tikz}
\usepackage{pgfplots}
\usetikzlibrary{decorations.pathreplacing,calc,arrows.meta,shapes,graphs}

\AtBeginEnvironment{minted}{\singlespacing\fontsize{10}{10}\selectfont}
\usefonttheme{serif}

\makeatletter
\patchcmd{\beamer@sectionintoc}{\vskip1.5em}{\vskip0.5em}{}{}
\makeatother

% Math stuffs
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\lcm}{\text{lcm}}
\newcommand{\Inn}{\text{Inn}}
\newcommand{\Aut}{\text{Aut}}
\newcommand{\Ker}{\text{Ker}\ }
\newcommand{\la}{\langle}
\newcommand{\ra}{\rangle}

\newcommand{\yournewcommand}[2]{Something #1, and #2}

\newenvironment{question}[1]{\par\textbf{Question #1.}\par}{}

\newcommand{\pmidg}[1]{\parbox{\widthof{#1}}{#1}}
\newcommand{\splitslide}[4]{
    \noindent
    \begin{minipage}{#1 \textwidth - #2 }
        #3
    \end{minipage}%
    \hspace{ \dimexpr #2 * 2 \relax }%
    \begin{minipage}{\textwidth - #1 \textwidth - #2 }
        #4
    \end{minipage}
}

\newcommand{\frameoutput}[1]{\frame{\colorbox{white}{#1}}}

\newcommand{\tikzmark}[1]{%
\tikz[baseline=-0.55ex,overlay,remember picture] \node[inner sep=0pt,] (#1)
{\vphantom{T}};
}

\newcommand{\braced}[3]{%
    \begin{tikzpicture}[overlay,remember picture]
        \draw [thick,decorate,decoration={brace,raise=1ex,amplitude=4pt},blue] (#2.south west-|T1.south west) -- node[anchor=west,left,xshift=-1.8ex,text=olive]{#3} (#1.north west-|T1.south west);
    \end{tikzpicture}
}

\newcommand{\muttrc}[0]{\texttt{\textasciitilde/.mutt/muttrc}\xspace}

\title{Mutt --- Terminal Email Client}
\author{Sumner Evans}
\institute{Mines Linux Users Group}

\begin{document}

\section{Overview}

\begin{frame}{What is Mutt?}
    \begin{quote}
         ``All mail clients suck. This one just sucks less.''

         --- Michael Elkins, circa 1995
    \end{quote}

    Mutt is a text-based email client. It's basically the Vim of email clients.

    It has revolutionary features such as\dots
    \begin{itemize}
        \item support for color terminals
        \item MIME and OpenPGP support
        \item threaded sorting mode
    \end{itemize}

    But seriously, it's actually a good email client.

\end{frame}

\begin{frame}{Reasons I use Mutt}
    The features that I like about Mutt are

    \begin{itemize}
        \item Scriptability and customizability of everything
        \item Vim bindings
        \item PGP/GPG support
        \item Support for multiple accounts
        \item Modularity
        \item Makes you look really cool
        \item Better than using \texttt{telnet}
    \end{itemize}

\end{frame}

\begin{frame}{General Usage}
    The usage of Mutt is extremely easy (most of the work goes into setup which
    I will cover later). All you have to do is run \texttt{mutt} from a
    terminal.
    \pause

    \textbf{Reading emails:} Open Mutt, then use arrow keys or vim bindings to
    go between messages. Press enter to view a message.
    \pause

    \textbf{Writing emails:} Press \texttt{m} and you will be prompted to enter
    who the email is to and a subject. Then you write your message using the
    editor and press \texttt{y} to send.
\end{frame}

\section{Setting up Mutt}

\begin{frame}{What you Need}

    Because Mutt is modular, you need to install programs to download your
    mail\footnote[frame]{Mutt can do this by default, but it does not support
        offline mail}, as well as send your mail.

    The most common programs for this are
    \begin{itemize}
        \item \texttt{offlineimap} for downloading your mail.
        \item \texttt{msmtp} for sending your mail.
    \end{itemize}

    Refer to David's
    presentation\footnote[frame]{https://gitlab.com/edwargix/mu4e-talk} for
    details on how to set up both \texttt{offlineimap} and \texttt{msmtp}.

\end{frame}

\begin{frame}{An offline alternative to \texttt{msmtp}}
    One of the main problems with \texttt{msmtp} is that it does not work
    offline. That means that you cannot send an email while offline! Luckily I
    wrote a program which allows you to do this:
    \texttt{offlinemsmtp}\footnote[frame]{https://gitlab.com/sumner/offlinemsmtp}
    (GPLv3).
    \pause

    \texttt{offlinemsmtp} runs as a daemon and listens for emails to appear in
    an outbox folder. It tries to send them if there is an internet connection,
    but if there isn't, it queues the message for later and will retry every
    minute.
    \pause

    All you have to do to use it is replace \texttt{msmtp} with
    \texttt{offlinemsmtp} in your configuration file.
\end{frame}

\begin{frame}[fragile]{Mutt Configuration File (\muttrc)}
    The main configuration file for Mutt is located at \muttrc. The following
    lines are required for Mutt to work.
    \inputminted{bash}{./examples/muttrc-minimal}
    \pause

    From here, the rest of the configurations are optional, quality-of-life
    configs.
\end{frame}

\begin{frame}[fragile]{Multiple Accounts}
    You can configure Mutt to associate folders with different accounts.
    \begin{minted}{bash}
        # Switching between accounts
        folder-hook Personal/*  source ~/.mutt/accounts/personal
        folder-hook Mines/*     source ~/.mutt/accounts/mines

        macro index M "<change-folder>=Mines/INBOX<enter>" \
            "go to Mines Inbox"
        macro index P "<change-folder>=Personal/INBOX<enter>" \
            "go to Personal Inbox"
    \end{minted}

\end{frame}

\begin{frame}[fragile]{Improvements for Gmail and Less Prompts}
    \begin{minted}{bash}
        unset move           # gmail does that
        set delete           # don't ask, just do
        unset confirmappend  # don't ask, just do!
        set quit             # don't ask, just do!!
        unset mark_old       # read/new is good enough for me


        macro index d \
            "<save-message>+Mines/[Gmail].Trash<enter>" \
            "move message to the trash"

        macro index gd "<delete-message>" \
            "delete the mesage permanently"

        macro index y \
            "<save-message>+Mines/[Gmail].All Mail<enter>" \
            "Archive"
    \end{minted}
\end{frame}

\begin{frame}[fragile]{Sorting}
    \begin{minted}{bash}
        set sort     = threads
        set sort_aux = reverse-last-date-received
        set sort_re
    \end{minted}

    If you are already in mutt, press \texttt{o} (little o).
\end{frame}

\begin{frame}[fragile]{Aliases}
    You can set up aliases so that you do not have to type out the full email of
    everyone you send mail to.

    \begin{minted}{bash}
        alias david     David Florness <davidflorness@mines.edu>
        alias jackson   Jack Garner <jgarner@mines.edu>
        ...
    \end{minted}

    You can even use this to define groups (it's also recursive!)
    \begin{minted}{bash}
        alias lugoff    jackson, jo, jordan, frick
    \end{minted}
\end{frame}

\begin{frame}[fragile]{Signatures}
    You can configure your signature like so:
    \begin{minted}{bash}
        set signature = "python3 ~/.mutt/signatures/personal|"
    \end{minted}

    Note the \texttt{|} at the end, that means that the signature should be
    executed. This is the script I use.

    \begin{minted}{python3}
        #!/usr/bin/python3
        from sig_helper import get_quote  # calls fortune on my quotesfile
        lines = [
            ('Jonathan Sumner Evans', 'Graduate Student, Computer Science'),
            ('https://sumnerevans.com', 'Chair, Mines ACM'),
            ('+1 (720) 459-1501', 'Service Chair, TBP'),
            ('GPG: B50022FD', 'Linux Help Guru, LUG'),
        ]

        print('\n'.join('{:32}{}'.format(*x) for x in lines))
        print()
        print(get_quote())
    \end{minted}
\end{frame}

\begin{frame}[fragile]{Display Filter}
    You can filter the output of each email to the buffer using a display
    filter.
    \begin{minted}{bash}
        set display_filter="~/bin/mutt-display-filter.py"
    \end{minted}

    The script gets all of the message in on \texttt{stdin} and whatever is
    output in \texttt{stdout} is shown.

    You can use this to do things like shorten links, parse datetimes, and any
    other shenanigans you can think of.
\end{frame}

\begin{frame}[fragile]{PGP/GPG}
    Mutt supports PGP/GPG out of the box. All you have to do is add these config
    options to your \muttrc.
    \begin{minted}{bash}
        set crypt_use_gpgme                 # Necessary to use GPG
        set crypt_autosign                  # Automatically sign all messages
        set crypt_opportunistic_encrypt     # Automatically encrypt if possible
        set crypt_verify_sig=yes            # Always verify signatures
        set pgp_self_encrypt    = yes       # encrypt to self
        set pgp_self_encrypt_as = B50022FD  # replace with your key
        set my_msmtp_pass=`pass show Mail/Offlineimap-Mines`
    \end{minted}
\end{frame}

\begin{frame}[standout]
    \Huge
    Demo
\end{frame}

\begin{frame}{References}
    \begin{itemize}
        \item My config:
            \href{https://gitlab.com/sumner/dotfiles/blob/master/.mutt/muttrc}{%
                \url{https://gitlab.com/sumner/dotfiles/blob/master/.mutt/muttrc}}
        \item The Mutt \texttt{man} pages: \texttt{man mutt} and \texttt{man
                muttrc}
        \item The Mutt website:
            \href{http://www.mutt.org/}{\url{http://www.mutt.org/}}
        \item The Mutt manual: \href{http://www.mutt.org/doc/manual/}{%
                \url{http://www.mutt.org/doc/manual/}}
    \end{itemize}
\end{frame}

\begin{frame}[standout]
    \Huge
    Questions?
\end{frame}

\end{document}
% Local Variables:
% TeX-command-extra-options: "-shell-escape"
% End:
